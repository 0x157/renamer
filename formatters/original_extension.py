from os.path import splitext
from base import DynamicFormatter

class OrigExt(DynamicFormatter):
    def __init__(self):
        pass

    def get(self, options={}):
        return splitext(options['file'])[1]

    @staticmethod
    def create(definition):
        if definition == 'origext':
            return OrigExt()
        else:
            return None

    @staticmethod
    def help():
        return '{origext}: original file extension (including dot) of the file to be renamed.'
