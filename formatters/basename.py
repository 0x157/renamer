from os.path import basename, splitext
from base import DynamicFormatter

class BaseName(DynamicFormatter):
    def __init__(self):
        pass

    def get(self, options={}):
        name = splitext(basename(options['file']))[0]
        if name is None or name == '':
            raise Exception('Failed to get basename, not a valid file path.')
        return name

    @staticmethod
    def create(definition):
        if definition == 'basename':
            return BaseName()
        else:
            return None

    @staticmethod
    def help():
        return '{basename}: basename (without file extension) of the file to be renamed.'
