from base import DynamicFormatter

class Counter(DynamicFormatter):

    def __init__(self, start=None, padding=None):
        self.padding = padding
        self.cntr = start if start is not None else 0

    def get(self, options={}):
        f = "{}" if self.padding is None else "{{:0>{}}}".format(self.padding)
        result = f.format(self.cntr)
        self.cntr += 1
        return result

    @staticmethod
    def getNumberForIdentifier(option, identifier):
        start = None
        end = None
        for i, s in enumerate(option):
            if s==identifier and start is None:
                start = i+1
            elif start is not None and s not in '0123456789':
                end = i
                break

        if start is not None and end is None:
            end = len(option)

        return int(option[start:end]) if start is not None else None

    @staticmethod
    def getStartValue(option):
        return Counter.getNumberForIdentifier(option, 's')

    @staticmethod
    def getPadding(option):
        return Counter.getNumberForIdentifier(option, 'p')

    @staticmethod
    def create(definition):
        l = len('cntr')
        if definition[:l] == 'cntr':
            option = definition[l:]
            if len(option)>1 and option[0]==':':
                startVal = Counter.getStartValue(option[1:])
                padding = Counter.getPadding(option[1:])
                return Counter(start=startVal, padding=padding)
            else:
                return Counter()
        else:
            return None

    @staticmethod
    def help():
        return "{cntr[:[s<Number>][p<Number>]]}: Specifies the usage of a counter that gets "\
                "incremented for each processed file. Default start value is 0."\
                "The counter has two optional values. "\
                "Start value: denoted by a 's' immediately followed by the start value. "\
                " Zero padding: denoted by a 'p' immediately followed by the padding value. "\
                "The counter gets left filled with zeros up to the length of the padding value."
