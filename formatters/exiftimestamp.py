from PIL import Image
from PIL.ExifTags import TAGS
from datetime import datetime
from base import DynamicFormatter

def getExifData(image):
    result = {}
    raw_exif = image._getexif()
    for raw_tag, value in raw_exif.items():
        tag = TAGS.get(raw_tag)
        if tag is not None:
            result[tag] = value
    return result

class ExifTimestamp(DynamicFormatter):
    def __init__(self, format):
        self.format = format

    def get(self, options={}):
        with Image.open(options['file']) as img:
            exif = getExifData(img)
        dt = datetime.strptime(exif['DateTime'], '%Y:%m:%d %H:%M:%S')
        return dt.strftime(self.format)

    @staticmethod
    def create(definition):
        if definition[:len('exiftime:')] == 'exiftime:':
            return ExifTimestamp(definition[len('exiftime:'):])
        else:
            return None

    @staticmethod
    def help():
        return "{exiftime:FORMAT}: exif creation timestamp of the picture to be renamed.\n"\
                "The formatting is according to strftime. See 'man strftime' or "\
                "https://docs.python.org/3.5/library/datetime.html#strftime-strptime-behavior"
