from base import DynamicFormatter
import re

class Reformat(DynamicFormatter):
    def __init__(self, old_match, new_name):
        self.old_match = old_match
        self.new_name = new_name

    def get(self, options={}):
        if len(self.old_match) > 0:
            m = re.search(self.old_match, options['file'])
            if m is None:
                raise Exception('reformat failed parsing file name: {}, re: {}'.format(options['file'], self.old_match))
            if len(m.groups()) > 0:
                return self.new_name.format(*m.groups())

        return self.new_name

    @staticmethod
    def split(formatters):
        for i in range(len(formatters)):
            if formatters[i] == '/' and not(i>0 and formatters[i-1] == '\\'):
                return (formatters[:i], formatters[i+1:])
        return None

    @staticmethod
    def create(definition):
        if definition[:len('reformat:')] == 'reformat:':
            formatters = Reformat.split(definition[len('reformat:'):])
            if formatters is None:
                return None
            return Reformat(formatters[0], formatters[1])
        else:
            return None

    @staticmethod
    def help():
        return """{reformat:old match/new name}: Replace the original file name with the
new name. You can specify a regex (old match) to extract information from the original
file name using python's regex group syntax. Use python's format syntax to add matched
groups, note that you have to escape '{' and '}'.
E.g.: {reformat:t(e)(s)t.*/new\{1\}\{0\}name}"""
