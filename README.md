Rename files using a specific pattern.

## Usage
The new file name pattern consists of fixed strings in combination with formatters.
Formatters are identified by: {\<formatter name\>[\<:options\>\]}.
Currently the following formatters exist:
- basename
- origext
- exiftime
- reformat
- counter

For details about the different formatters check the help page:  
```python3 renamer.py --help```

## Example
For example use exif timestamps in the new name:  
```python3 renamer.py 'new_file_name-{exiftime:%Y%m%d%H%M%S}{origext}' test.jpg```  
New file name: ```new_file_name-20180917124831.jpg```

## Dependencies
See ```requirements.txt```.
