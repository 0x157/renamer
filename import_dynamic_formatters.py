from os.path import abspath, basename, dirname, isfile, join
import sys
currentdir = dirname(abspath(__file__))
formattersdir = join(currentdir, "formatters")
sys.path.insert(0, formattersdir)

import glob
import importlib
import inspect
from base import DynamicFormatter

def getFormatters():
    formatters = []
    files = glob.glob(formattersdir + "/*.py")

    for f in files:
        if isfile(f) and not f.endswith('__init__.py'):
            module = importlib.import_module(basename(f)[:-3])
            for name, obj in inspect.getmembers(module):
                if inspect.isclass(obj)\
                        and issubclass(obj, DynamicFormatter)\
                        and not obj == DynamicFormatter:
                    formatters.append(obj)

    return formatters
