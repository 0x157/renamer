import click
import import_dynamic_formatters
import os
import shutil

class StaticName:
    def __init__(self, name):
        self.name = name

    def get(self, options={}):
        return self.name

def toDynamicParser(definition):
    dynamicParsers = import_dynamic_formatters.getFormatters()
    for c in dynamicParsers:
        p = c.create(definition)
        if p is not None:
            return p

    raise Exception('no matching parser found')

def getDynamicPart(name, pos):
    i = pos+1
    end = len(name)
    dynamic = ''
    while i < end:
        if name[i] == '\\':
            i += 1
        elif name[i] == '}':
            break
        dynamic += name[i]
        i += 1

    if i == end:
        raise Exception('no closing \'}\' for dynamic part')

    return toDynamicParser(dynamic), i

def parseFileName(name):
    fileNameStructure = []
    static = ''
    i = 0
    end = len(name)
    while i < end:
        if name[i] == '\\':
            i += 1
            if i == end:
                raise Exception('unexpected escape character')
            static += name[i]
        elif name[i] == '{':
            if len(static) > 0:
                fileNameStructure.append(StaticName(static))
                static = ''
            dynamic, i = getDynamicPart(name, i)
            fileNameStructure.append(dynamic)
        else:
            static += name[i]
        i += 1

    if len(static) > 0:
        fileNameStructure.append(StaticName(static))
        static = ''

    return fileNameStructure

def getDstName(input_file, formatter_list, copy_dir):
    path, basename = os.path.split(input_file)
    output = "{}/".format(copy_dir) if copy_dir is not None else "{}/".format(path) if len(path) > 0 else ""
    for formatter in formatter_list:
        output += formatter.get({'file': input_file})

    return output

def getDynamicFormattersHelp():
    dynamicParsers = import_dynamic_formatters.getFormatters()
    helpMsg = "" if len(dynamicParsers) == 0 else dynamicParsers[0].help()
    for parser in dynamicParsers[1:]:
        helpMsg += "\n\n"
        helpMsg += parser.help()

    return helpMsg

def renamerHelp():
    help = """
OUPUT_FORMAT specifies the format of the file's new name.
The format can consists of static and dynamic parts.
Dynamc formatters are put into curly braces ('{{dynamic format}}').
If you want to use '{{' or '}}' in the ouput name, you can use '\\' to escape them.
The following dynamic formatters are supported:

{}

\b
Example:
  python renamer.py '{{exiftime:%Y%m%d%H%M%S}}_{{basename}}{{origext}}' test.jpg
  test.jpg -> 20180917124831_test.jpg
""".format(getDynamicFormattersHelp())

    return help

@click.command(help=renamerHelp())
@click.argument('output_format')
@click.argument('inputs', nargs=-1)
@click.option('--copy_dir', '-c', help="Does not rename any files, but instead copies the input files to the provide directory using the output file name format.")
@click.option('--dry_run', is_flag=True, help="Prints what would be done be the rename operations")
def main(output_format, inputs, copy_dir, dry_run):

    formatter_list = parseFileName(output_format)
    for src in inputs:
        dst = getDstName(src, formatter_list, copy_dir)
        if dry_run:
            print('{} -> {}'.format(src, dst))
        elif copy_dir is not None:
            shutil.copyfile(src, dst)
        else:
            shutil.move(src, dst)

if __name__ == '__main__':
    main()
