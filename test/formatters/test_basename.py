import os,sys
currentdir = os.path.dirname(os.path.abspath(__file__))
formattersdir = os.path.join(currentdir, "..", "..", "formatters")
sys.path.insert(0, formattersdir)

import unittest
from basename import BaseName

class TestBaseName(unittest.TestCase):

    def test_create_success(self):
        self.assertIsNotNone(BaseName.create('basename'))

    def test_create_fail(self):
        self.assertIsNone(BaseName.create('not_a_basename'))

    def test_help(self):
        helpMsg = BaseName.help()
        self.assertIsNotNone(helpMsg)
        self.assertNotEqual(helpMsg, '')

    def test_get(self):
        base = BaseName.create('basename')
        self.assertEqual(base.get({'file': 'hello.ext'}), 'hello')
        self.assertEqual(base.get({'file': 'hello.'}), 'hello')
        self.assertEqual(base.get({'file': 'hello'}), 'hello')
        self.assertEqual(base.get({'file': 'path/to/file/hello.ext'}), 'hello')
        self.assertEqual(base.get({'file': 'path/to/file/hello.'}), 'hello')
        self.assertEqual(base.get({'file': 'path/to/file/hello'}), 'hello')
        self.assertEqual(base.get({'file': 'path/to/file/.hello'}), '.hello')

        with self.assertRaises(Exception) as e:
            base.get({'file': 'path/to/file/hello/'})
        self.assertEqual(str(e.exception), 'Failed to get basename, not a valid file path.')


if __name__ == '__main__':
    unittest.main()
