import os,sys
currentdir = os.path.dirname(os.path.abspath(__file__))
formattersdir = os.path.join(currentdir, "..", "..", "formatters")
sys.path.insert(0, formattersdir)

import unittest
from reformat import Reformat

class TestReformat(unittest.TestCase):

    def test_create_success(self):
        # test for an empty formatter
        self.assertIsNotNone(Reformat.create('reformat:/'))

    def test_create_fail(self):
        self.assertIsNone(Reformat.create('not_a_reformat'))

    def test_help(self):
        helpMsg = Reformat.help()
        self.assertIsNotNone(helpMsg)
        self.assertNotEqual(helpMsg, '')

    def test_get(self):
        base = Reformat.create('reformat:/new_file')
        self.assertEqual(base.get({'file': 'hello.ext'}), 'new_file')

        # escaping '{' and '}' is not neccessary for tests.
        # only required for renamer, because those charecters distinguis differen formatters.
        base = Reformat.create('reformat:hello([0-9]+).*/new_file{0}')
        self.assertEqual(base.get({'file': 'hello23.'}), 'new_file23')
        self.assertEqual(base.get({'file': 'hello0'}), 'new_file0')
        self.assertEqual(base.get({'file': 'hello0_45h'}), 'new_file0')

        with self.assertRaises(Exception) as e:
            base.get({'file': 'not_matching123_'})
        self.assertEqual(str(e.exception),
                'reformat failed parsing file name: not_matching123_, re: hello([0-9]+).*')


if __name__ == '__main__':
    unittest.main()
