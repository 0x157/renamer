import os,sys
currentdir = os.path.dirname(os.path.abspath(__file__))
formattersdir = os.path.join(currentdir, "..", "..", "formatters")
sys.path.insert(0, formattersdir)

import unittest
from unittest.mock import patch
from exiftimestamp import ExifTimestamp, getExifData

class MockImageContext:
    def __init__(self, img):
        self.img = img

    def __enter__(self):
        return self.img

    def __exit__(self, except_type, except_value, except_traceback):
        pass

class MockImage:
    def __init__(self, mapping):
        self.mapping = mapping

    def _getexif(self):
        return self.mapping

class TestExifTimestamp(unittest.TestCase):

    def setUp(self):
        # 306 -> DateTime
        self.imgCtxForTesting = MockImageContext(MockImage({306: '2019:04:13 17:24:00'}))

    def test_create_success(self):
        self.assertIsNotNone(ExifTimestamp.create('exiftime:'))

    def test_create_fail(self):
        self.assertIsNone(ExifTimestamp.create('not_a_exiftime'))

    def test_help(self):
        helpMsg = ExifTimestamp.help()
        self.assertIsNotNone(helpMsg)
        self.assertNotEqual(helpMsg, '')

    def test_get(self):
        base = ExifTimestamp.create('exiftime:%Y/%m/%d')
        with patch('exiftimestamp.Image.open') as mocked_open:
            mocked_open.return_value = self.imgCtxForTesting
            self.assertEqual(base.get({'file': 'image.jpg'}), '2019/04/13')

    def test_getExifDataInvalidExifTag(self):
        data = getExifData(MockImage({1234567890: ''}))
        self.assertEqual(len(data), 0)


if __name__ == '__main__':
    unittest.main()
