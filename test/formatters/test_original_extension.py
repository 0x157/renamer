import os,sys
currentdir = os.path.dirname(os.path.abspath(__file__))
formattersdir = os.path.join(currentdir, "..", "..", "formatters")
sys.path.insert(0, formattersdir)

import unittest
from original_extension import OrigExt

class TestOrigExt(unittest.TestCase):

    def test_create_success(self):
        self.assertIsNotNone(OrigExt.create('origext'))

    def test_create_fail(self):
        self.assertIsNone(OrigExt.create('not_a_origext'))

    def test_help(self):
        helpMsg = OrigExt.help()
        self.assertIsNotNone(helpMsg)
        self.assertNotEqual(helpMsg, '')

    def test_get(self):
        base = OrigExt.create('origext')
        self.assertEqual(base.get({'file': 'hello.ext'}), '.ext')
        self.assertEqual(base.get({'file': 'hello.'}), '.')
        self.assertEqual(base.get({'file': 'hello'}), '')
        self.assertEqual(base.get({'file': 'path/to/file/hello.ext'}), '.ext')
        self.assertEqual(base.get({'file': 'path/to/file/hello.'}), '.')
        self.assertEqual(base.get({'file': 'path/to/file/hello'}), '')
        self.assertEqual(base.get({'file': 'path/to/file/.hello'}), '')


if __name__ == '__main__':
    unittest.main()
