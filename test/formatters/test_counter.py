import os,sys
currentdir = os.path.dirname(os.path.abspath(__file__))
formattersdir = os.path.join(currentdir, "..", "..", "formatters")
sys.path.insert(0, formattersdir)

import unittest
from counter import Counter

class TestCounter(unittest.TestCase):

    def test_create_default(self):
        c = Counter.create('cntr')
        self.assertIsNotNone(c)
        self.assertIsNone(c.padding)
        self.assertEqual(c.cntr, 0)

        c = Counter.create('cntr:')
        self.assertIsNotNone(c)
        self.assertIsNone(c.padding)
        self.assertEqual(c.cntr, 0)

    def test_create_with_padding(self):
        c = Counter.create('cntr:p5')
        self.assertIsNotNone(c)
        self.assertEqual(c.padding, 5)
        self.assertEqual(c.cntr, 0)

    def test_create_with_start_val(self):
        c = Counter.create('cntr:s42')
        self.assertIsNotNone(c)
        self.assertIsNone(c.padding)
        self.assertEqual(c.cntr, 42)

    def test_create_with_padding_and_start_val(self):
        c = Counter.create('cntr:s42p5')
        self.assertIsNotNone(c)
        self.assertEqual(c.padding, 5)
        self.assertEqual(c.cntr, 42)

        c = Counter.create('cntr:p7s43')
        self.assertIsNotNone(c)
        self.assertEqual(c.padding, 7)
        self.assertEqual(c.cntr, 43)

    def test_create_fail(self):
        self.assertIsNone(Counter.create('not_a_counter'))

    def test_help(self):
        helpMsg = Counter.help()
        self.assertIsNotNone(helpMsg)
        self.assertNotEqual(helpMsg, '')

    def test_get(self):
        c = Counter.create('cntr')
        self.assertEqual(c.get(), '0')
        self.assertEqual(c.get(), '1')
        self.assertEqual(c.get(), '2')

    def test_get_padding(self):
        c = Counter.create('cntr:p5')
        self.assertEqual(c.get(), '00000')
        self.assertEqual(c.get(), '00001')
        self.assertEqual(c.get(), '00002')

    def test_get_custom_start_value(self):
        c = Counter.create('cntr:s42')
        self.assertEqual(c.get(), '42')
        self.assertEqual(c.get(), '43')
        self.assertEqual(c.get(), '44')
        self.assertEqual(c.get(), '45')

    def test_get_padding_and_custom_start_value(self):
        c = Counter.create('cntr:p3s8')
        self.assertEqual(c.get(), '008')
        self.assertEqual(c.get(), '009')
        self.assertEqual(c.get(), '010')
        self.assertEqual(c.get(), '011')


if __name__ == '__main__':
    unittest.main()
