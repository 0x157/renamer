import unittest
import sys

# import your test modules
from formatters.test_basename import TestBaseName
from formatters.test_original_extension import TestOrigExt
from formatters.test_reformat import TestReformat
from formatters.test_exiftimestamp import TestExifTimestamp
from formatters.test_counter import TestCounter

# initialize the test suite
loader = unittest.TestLoader()
suite  = unittest.TestSuite()

# add tests to the test suite
suite.addTests(loader.loadTestsFromTestCase(TestBaseName))
suite.addTests(loader.loadTestsFromTestCase(TestOrigExt))
suite.addTests(loader.loadTestsFromTestCase(TestReformat))
suite.addTests(loader.loadTestsFromTestCase(TestExifTimestamp))
suite.addTests(loader.loadTestsFromTestCase(TestCounter))

# initialize a runner, pass it your suite and run it
runner = unittest.TextTestRunner(verbosity=0)
result = runner.run(suite)

sys.exit(not result.wasSuccessful())
